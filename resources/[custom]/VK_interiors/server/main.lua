ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('vkinteriors:logInteriorEntry')
AddEventHandler('vkinteriors:logInteriorEntry', function(interiorName) 
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    
    sendToDiscord(xPlayer.name .. ' ' .. interiorName)
end)

function sendToDiscord (message)
    local DiscordWebHook = Config.webhook
    local name = 'Interieur betreden/verlaten'
    -- Modify here your discordWebHook username = name, content = message,embeds = embeds
  
  local embeds = {
      {
          ["title"]=message,
          ["type"]="rich",
          ["footer"]=  {
              ["text"]= "Interieur Logs",
         },
      }
  }
  
    if message == nil or message == '' then return FALSE end
    PerformHttpRequest(DiscordWebHook, function(err, text, headers) end, 'POST', json.encode({ username = name,embeds = embeds}), { ['Content-Type'] = 'application/json' })
end