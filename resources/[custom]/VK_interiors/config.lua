INTERIORS = {
    -- Maffia
    --[1] = {id = 1, x = 1395.14, y = 1141.96, z = 114.63, h = 82.63, name = "Clubhuis Verlaten", destination = {2}},
    --[2] = {id = 2, x = 996.9, y = -3158.08, z = -38.91, h = 268.78, name = "Clubhuis Binnengaan", destination = {1}},
	-- Weed
	[1] = {id = 1, x = 722.64, y = 2330.75, z = 51.95, h = 10.39, name = "Weed verwerking verlaten", destination = {2}},
    [2] = {id = 2, x = 1066.32, y = -3183.4, z = -39.16, h = 92.97, name = "Weed verwerking betreden", destination = {1}},
	-- Meth
	[3] = {id = 3, x = 1997.318, y = 3039.371, z = 47.026, h = 268.34, name = "Meth verwerking verlaten", destination = {4}},
    [4] = {id = 4, x = 996.505, y = -3200.581, z = -36.993, h = 268.34, name = "Meth verwerking betreden", destination = {3}},
	-- Coke
	[5] = {id = 5, x = 1088.63, y = -3187.49, z = -38.99, h = 170.47, name = "Coke verwerking betreden", destination = {6}},
    [6] = {id = 6, x = 474.15, y = -1951.67, z = 24.70, h = 170.47, name = "Coke verwerking verlaten", destination = {5}},
	-- XTC
	[7] = {id = 7, x = 1121.0605, y = -3152.733, z = -36.97, h = 100, name = "XTC verwerking betreden", destination = {8}},
    [8] = {id = 8, x = 839.51, y = 2176.79, z = 52.30, h = 100, name = "XTC verwerking verlaten", destination = {7}},
	-- HTCT
	[9] = {id = 9, x = 2022.5653076172, y = 3020.7133789063, z = -72.691757202148, h = 36.087, name = "HTCT verlaten", destination = {10}},
    [10] = {id = 10, x = -332.89059448242, y = -1067.0101318359, z = 23.025814056396, h = 341.86947631836, name = "HTCT betreden", destination = {9}},
	-- Rechtbank
	[11] = {id = 11, x = 237.89, y = -412.91, z = 48.12, h = 337, name = "Rechtbank verlaten", destination = {12}},
    [12] = {id = 12, x = 236.674, y = -400.48, z = -84.93, h = 87.42, name = "Rechtbank betreden", destination = {11}},
	[13] = {id = 13, x = 232.92, y = -411.36, z = 48.12, h = 337, name = "Rechtbank verlaten", destination = {12}},
}
Config = {}
Config.webhook = "https://discordapp.com/api/webhooks/625416785801379841/jODNNKfY7oTmG9xIhqPDEayT0lHP8JO2KD9Q2W18SdzJa8KjEIIVOtcdo8HYEMwbi6TZ"
Config.enableInteriorLogs = true