CREATE DATABASE IF NOT EXISTS `essentialmode` 
USE `essentialmode`;

INSERT INTO `items` (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES
	('a3paper', 'A3 Paper', 500, 0, 1),
	('a4paper', 'A4 Paper', 500, 0, 1),
	('a5paper', 'A5 Paper', 500, 0, 1),
	('cotton_fibers', 'Cutton Fibers', 50000, 0, 1),
     ('a3money_print_machine', 'A3 Money Print Machine', 1, 0, 1),
     ('a4money_print_machine', 'A4 Money Print Machine', 1, 0, 1),
     ('a5money_print_machine', 'A5 Money Print Machine', 1, 0, 1),
     ('fake_money50', 'Fake €50 paper', 500, 0, 1),
     ('fake_money20', 'Fake €20 paper', 500, 0, 1),
     ('fake_money5', 'Fake €5 paper', 500, 0, 1),
     ('fake_biljet50', 'Fake €50 biljet', 5000, 0, 1),
     ('fake_biljet20', 'Fake €20 biljet', 5000, 0, 1),
     ('fake_biljet5', 'Fake €5 biljet', 5000, 0, 1),
     ('electricity_cable5m', 'Verlengkabel 5m', 5, 0, 1),
     ('electricity_cable10m', 'Verlengkabel 10m', 5, 0, 1),
     ('electricity_cable20m', 'Verlengkabel 20m', 5, 0, 1);
     