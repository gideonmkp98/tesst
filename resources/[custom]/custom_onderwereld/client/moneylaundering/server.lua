ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('moneyLaundering:launder')
AddEventHandler('moneyLaundering:launder', function(amount, tax)
    local xPlayer = ESX.GetPlayerFromId(source, tax)
    amount = ESX.Math.Round(tonumber(amount))
    washedTotal = ESX.Math.Round(tonumber(amount * tax))
    TriggerClientEvent('main:notify', source, _U('laundery_finished',washedTotal))
    if xPlayer.getAccount('black_money').money >= amount then
      xPlayer.removeAccountMoney('black_money', amount)
      xPlayer.addMoney(washedTotal)
    end
end)