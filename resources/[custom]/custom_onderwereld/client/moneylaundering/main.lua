ESX = nil
local PlayerData = {}
local HasAlreadyEnteredMarker = false
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

menuOpen = false

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(4)
        local isInMarker = false
        local coords = GetEntityCoords(PlayerPedId())
        
        for k,v in pairs(Config.MoneyLaundering) do
            if GetDistanceBetweenCoords(coords, v.coords, true) < 1.0 then
                isInMarker = true
                HasAlreadyEnteredMarker = true
                if not menuOpen then
                    ESX.ShowHelpNotification(_U('open_moneylaundering'))
                    if IsControlJustReleased(0, Config.Keys['E']) then
                        MoneyAmountSelect(v.tax)
                        menuOpen = true
                    end
                end
            end	
        end 
    end
end)

function MoneyAmountSelect(tax)
    ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'lookup_vehicle', {
		title = _U('amount_money')
	}, function(data, menu)
		if tonumber(data.value) == nil or data.value == nil then
			print('fout kanker debiel')
		else
            TriggerServerEvent('moneyLaundering:launder', data.value, tax)
            menuOpen = false
		end
        menu.close()
        menuOpen = false
	end, function(data, menu)
        menu.close()
        menuOpen = false
	end)
end