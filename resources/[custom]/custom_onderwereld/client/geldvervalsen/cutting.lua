isCutting = false

Citizen.CreateThread(function()
     while true do
          Citizen.Wait(4)
          local playerCoords = GetEntityCoords(PlayerPedId()) 

          if GetDistanceBetweenCoords(playerCoords, Config.Circles.PaperCutting.coords, true) < 1.0 then
               if not isCutting then
                    ESX.ShowHelpNotification(_U('start_cutting'))
               end
               if IsControlJustReleased(0, Config.Keys['E']) then
                    OpenPaperCuttingMenu()
               end
          end

          if isCutting then
               if GetDistanceBetweenCoords(playerCoords, Config.Circles.PaperCutting.coords, true) < 2.0 then
                    isCutting = true
               else
                    isCutting = false
                    TriggerServerEvent('processing:stop')
               end
          end

     end
end)

function OpenPaperCuttingMenu()
     ESX.UI.Menu.CloseAll()

     ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'paper_printing', {
		title    =  _U('paper_cutting'),
		align    = 'top-right',
		elements = {
               { label = 'A3', value = 'a3' },
               { label = 'A4', value = 'a4' },
               { label = 'A5', value = 'a5' },
          }
	}, function(data, menu)
		menu.close()
          menuOpen = false
          if data.current.value == 'a3' then
               TriggerServerEvent('cutting:starta3')
               isCutting = true
		end
		if data.current.value == 'a4' then
               TriggerServerEvent('cutting:starta4')
               isCutting = true
          end
          if data.current.value == 'a5' then
               TriggerServerEvent('cutting:starta5')
               isCutting = true
          end
          Citizen.Wait(4)
	end)
end