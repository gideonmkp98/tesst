isProcessing = false

Citizen.CreateThread(function()
     while true do
          Citizen.Wait(4)
          local playerCoords = GetEntityCoords(PlayerPedId()) 

          if GetDistanceBetweenCoords(playerCoords, Config.Circles.PaperProcessing.coords, true) < 1.0 then
               if not isProcessing then
                    ESX.ShowHelpNotification(_U('start_paperprocessing'))
               end
               if IsControlJustReleased(0, Config.Keys['E']) then
                    OpenPaperProcessingMenu()
                    isProcessing = true
               end
          end

          if isProcessing then
               if GetDistanceBetweenCoords(playerCoords, Config.Circles.PaperProcessing.coords, true) < 3.0 then
                    isProcessing = true
               else
                    isProcessing = false
                    TriggerServerEvent('processing:stop')
               end
          end

     end
end)

function OpenPaperProcessingMenu()
     ESX.UI.Menu.CloseAll()

     ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'paper_processing', {
		title    =  _U('paper_processing'),
		align    = 'top-right',
		elements = {
               { label = 'A3', value = 'a3' },
               { label = 'A4', value = 'a4' },
               { label = 'A5', value = 'a5' },
          }
	}, function(data, menu)
		menu.close()
          menuOpen = false
          if data.current.value == 'a3' then
               TriggerServerEvent('processing:starta3')
		end
		if data.current.value == 'a4' then
			TriggerServerEvent('processing:starta4')
          end
          if data.current.value == 'a5' then
			TriggerServerEvent('processing:starta5')
          end
          Citizen.Wait(4)
	end)
end

RegisterNetEvent('custom_onderwereld:notify')
AddEventHandler('custom_onderwereld:notify', function(message)
	ESX.ShowNotification(message)
end)
