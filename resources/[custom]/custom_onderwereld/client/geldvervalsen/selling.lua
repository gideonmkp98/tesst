isSelling = false

Citizen.CreateThread(function()
     while true do
          Citizen.Wait(4)
          local playerCoords = GetEntityCoords(PlayerPedId()) 

          if GetDistanceBetweenCoords(playerCoords, Config.Circles.FakeMoneySelling.coords, true) < 1.0 then
               if not isSelling then
                    ESX.ShowHelpNotification(_U('start_selling'))
               end
               if IsControlJustReleased(0, Config.Keys['E']) then
                    OpenPaperSellingMenu()
               end
          end

          if isSelling then
               if GetDistanceBetweenCoords(playerCoords, Config.Circles.FakeMoneySelling.coords, true) < 2.0 then
                    isSelling = true
               else
                    isSelling = false
                    TriggerServerEvent('processing:stop')
               end
          end

     end
end)

function OpenPaperSellingMenu()
     ESX.UI.Menu.CloseAll()

     ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'paper_printing', {
		title    =  _U('paper_selling'),
		align    = 'top-right',
		elements = {
               { label = '€5', value = '5', itemName = 'fake_biljet5' },
               { label = '€20', value = '20', itemName = 'fake_biljet20' },
               { label = '€50', value = '50', itemName = 'fake_biljet50' },
          }
	}, function(data, menu)
		menu.close()
          menuOpen = false
          AmountSelect(data.current.value, data.current.itemName)
	end)
end

function AmountSelect(value, itemName)
     ESX.UI.Menu.CloseAll()
	ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'lookup_vehicle', {
		title = 'Hoeveel geld wil je wit wasen?',
	}, function(data, menu)
		menuOpen = false
		menu.close()   
		local length = string.len(data.value)
		if data.value == nil or length < 1 or length > 13 then
			ESX.ShowHelpNotification("Dit is geen geldige invoer")
		else
               TriggerServerEvent('fakemoney:selling', data.value, value, itemName)
		end
	end, function(data, menu)
		menu.close()
		menuOpen = false
	end)
end

RegisterCommand('removeblackmoney', function()
     TriggerServerEvent('remove:blackmoney')
end)