RegisterServerEvent('processing:starta3')
AddEventHandler('processing:starta3', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
     hasItem = true
     --if hasItem then
     
     while hasItem do
          if xPlayer.getInventoryItem('cotton_fibers').count >= 50 then
               if xPlayer.getInventoryItem('a3paper').count >= 500 then
                   TriggerClientEvent('custom_onderwereld:notify', _source, _U('cant_hold_paper'))
                   hasItem = false
               else
                    TriggerClientEvent('custom_onderwereld:notify', _source, _U('processing_paper'))
                    xPlayer.removeInventoryItem('cotton_fibers', 200)
                    xPlayer.addInventoryItem('a3paper', 1)
               end
          else
               TriggerClientEvent('custom_onderwereld:notify', _source, _U('no_supplies_process'))
               hasItem = false
          end
          Wait(3000)
     --end
     end
end)

RegisterServerEvent('processing:starta4')
AddEventHandler('processing:starta4', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
     hasItem = true
     
     while hasItem do
          if xPlayer.getInventoryItem('cotton_fibers').count >= 50 then
               if xPlayer.getInventoryItem('a4paper').count >= 500 then
                   TriggerClientEvent('custom_onderwereld:notify', _source, _U('cant_hold_paper'))
                   hasItem = false
               else
                    TriggerClientEvent('custom_onderwereld:notify', _source, _U('processing_paper'))
                    xPlayer.removeInventoryItem('cotton_fibers', 200)
                    xPlayer.addInventoryItem('a4paper', 1)
               end
          else
               TriggerClientEvent('custom_onderwereld:notify', _source, _U('no_supplies_process'))
               hasItem = false
          end
          Wait(2000)
     --end
     end
end)

RegisterServerEvent('processing:starta5')
AddEventHandler('processing:starta5', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
     hasItem = true

     while hasItem do
          if xPlayer.getInventoryItem('cotton_fibers').count >= 50 then
               if xPlayer.getInventoryItem('a5paper').count >= 500 then
                   TriggerClientEvent('custom_onderwereld:notify', _source, _U('cant_hold_paper'))
                   hasItem = false
               else
                    TriggerClientEvent('custom_onderwereld:notify', _source, _U('processing_paper'))
                    xPlayer.removeInventoryItem('cotton_fibers', 200)
                    xPlayer.addInventoryItem('a5paper', 1)
               end
          else
               TriggerClientEvent('custom_onderwereld:notify', _source, _U('no_supplies_process'))
               hasItem = false
          end
          Wait(1000)
     --end
     end
end)

RegisterServerEvent('processing:stop')
AddEventHandler('processing:stop', function()
     hasItem = false
end)


RegisterServerEvent('printing:starta3')
AddEventHandler('printing:starta3', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
     hasItem = true
     
     while hasItem do
	     if xPlayer.getInventoryItem('a3paper').count >= 1 and xPlayer.getInventoryItem('a3money_print_machine').count >= 1 then
		     if xPlayer.getInventoryItem('fake_money50').count >= 500 then
				TriggerClientEvent('custom_onderwereld:notify', _source, _U('cant_hold_paper'))
		     else
			     TriggerClientEvent('custom_onderwereld:notify', _source, _U('printing_money'))
                    xPlayer.removeInventoryItem('a3paper', 1)
                    xPlayer.addInventoryItem('fake_money50', 1)
               end
	     else
		     TriggerClientEvent('custom_onderwereld:notify', _source, _U('no_supplies_printing'))
          end
          Wait(4000)
	end
end)

RegisterServerEvent('printing:starta4')
AddEventHandler('printing:starta4', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
     hasItem = true
     
     while hasItem do
	     if xPlayer.getInventoryItem('a4paper').count >= 1 and xPlayer.getInventoryItem('a4money_print_machine').count >= 1 then
		     if xPlayer.getInventoryItem('fake_money20').count >= 500 then
				TriggerClientEvent('custom_onderwereld:notify', _source, _U('cant_hold_paper'))
		     else
			     TriggerClientEvent('custom_onderwereld:notify', _source, _U('printing_money'))
                    xPlayer.removeInventoryItem('a4paper', 1)
                    xPlayer.addInventoryItem('fake_money20', 1)
               end
	     else
		     TriggerClientEvent('custom_onderwereld:notify', _source, _U('no_supplies_printing'))
          end
          Wait(4000)
	end
end)

RegisterServerEvent('printing:starta5')
AddEventHandler('printing:starta5', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
     hasItem = true
     
     while hasItem do
	     if xPlayer.getInventoryItem('a5paper').count >= 1 and xPlayer.getInventoryItem('a5money_print_machine').count >= 1 then
		     if xPlayer.getInventoryItem('fake_money5').count >= 500 then
				TriggerClientEvent('custom_onderwereld:notify', _source, _U('cant_hold_paper'))
		     else
			     TriggerClientEvent('custom_onderwereld:notify', _source, _U('printing_money'))
                    xPlayer.removeInventoryItem('a5paper', 1)
                    xPlayer.addInventoryItem('fake_money5', 1)
               end
	     else
		     TriggerClientEvent('custom_onderwereld:notify', _source, _U('no_supplies_printing'))
          end
          Wait(4000)
	end
end)

RegisterServerEvent('printing:cable_broke')
AddEventHandler('printing:cable_broke', function()
     local _source = source
     local xPlayer = ESX.GetPlayerFromId(_source)
     hasItem = false
     TriggerClientEvent('custom_onderwereld:notify', source, _U('cable_broke'))
     if xPlayer.getInventoryItem('electricity_cable20m').count >= 1 then
          xPlayer.removeInventoryItem('electricity_cable20m', 1)
     elseif xPlayer.getInventoryItem('electricity_cable10m').count >= 1 then
           xPlayer.removeInventoryItem('electricity_cable10m', 1)
     elseif xPlayer.getInventoryItem('electricity_cable5m').count >= 1 then
          xPlayer.removeInventoryItem('electricity_cable5m', 1)
     end
end)

ESX.RegisterServerCallback('falsifyMoney:checkCableLenght', function(source, cb)
     local _source = source
     local xPlayer = ESX.GetPlayerFromId(_source)
     if xPlayer.getInventoryItem('electricity_cable20m').count >= 1 then
          cb(20)
     elseif xPlayer.getInventoryItem('electricity_cable10m').count >= 1 then
          cb(10)
     elseif xPlayer.getInventoryItem('electricity_cable5m').count >= 1 then
          cb(5)
     else
          cb(nil)
     end
end)

RegisterServerEvent('cutting:starta3')
AddEventHandler('cutting:starta3', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
     hasItem = true
     
     while hasItem do
	     if xPlayer.getInventoryItem('fake_money50').count >= 1 then
		     if xPlayer.getInventoryItem('fake_biljet50').count >= 5000 then
				TriggerClientEvent('custom_onderwereld:notify', _source, _U('cant_hold_paper'))
		     else
			     TriggerClientEvent('custom_onderwereld:notify', _source, _U('cutting_money'))
                    xPlayer.removeInventoryItem('fake_money50', 1)
                    xPlayer.addInventoryItem('fake_biljet50', 10)
               end
	     else
		     TriggerClientEvent('custom_onderwereld:notify', _source, _U('no_supplies_cutting'))
          end
          Wait(3000)
	end
end)

RegisterServerEvent('cutting:starta4')
AddEventHandler('cutting:starta4', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
     hasItem = true
     
     while hasItem do
	     if xPlayer.getInventoryItem('fake_money20').count >= 1 then
		     if xPlayer.getInventoryItem('fake_biljet20').count >= 5000 then
				TriggerClientEvent('custom_onderwereld:notify', _source, _U('cant_hold_paper'))
		     else
			     TriggerClientEvent('custom_onderwereld:notify', _source, _U('cutting_money'))
                    xPlayer.removeInventoryItem('fake_money20', 1)
                    xPlayer.addInventoryItem('fake_biljet20', 6)
               end
	     else
		     TriggerClientEvent('custom_onderwereld:notify', _source, _U('no_supplies_cutting'))
          end
          Wait(3000)
	end
end)

RegisterServerEvent('cutting:starta5')
AddEventHandler('cutting:starta5', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
     hasItem = true
     
     while hasItem do
	     if xPlayer.getInventoryItem('fake_money5').count >= 1 then
		     if xPlayer.getInventoryItem('fake_biljet5').count >= 5000 then
				TriggerClientEvent('custom_onderwereld:notify', _source, _U('cant_hold_paper'))
		     else
			     TriggerClientEvent('custom_onderwereld:notify', _source, _U('cutting_money'))
                    xPlayer.removeInventoryItem('fake_money5', 1)
                    xPlayer.addInventoryItem('fake_biljet5', 4)
               end
	     else
		     TriggerClientEvent('custom_onderwereld:notify', _source, _U('no_supplies_cutting'))
          end
          Wait(3000)
	end
end)

RegisterServerEvent('fakemoney:selling')
AddEventHandler('fakemoney:selling', function(amount, value, itemName)
     local xPlayer = ESX.GetPlayerFromId(source)
     totalMoney = amount * value
     blackMoney = totalMoney * 0.6
     if xPlayer.getInventoryItem(itemName).count >= 1 then
          xPlayer.removeInventoryItem(itemName, amount)
          xPlayer.addAccountMoney('black_money', tonumber(blackMoney))
     end
end)

RegisterServerEvent('remove:blackmoney')
AddEventHandler('remove:blackmoney', function()
     local xPlayer = ESX.GetPlayerFromId(source)
     local playerBlackMoney = xPlayer.getAccount('black_money').money

     xPlayer.removeAccountMoney('black_money', playerBlackMoney)
end)