connected = false
isPrinting = false
local itemRemoved = false

Citizen.CreateThread(function()
     while true do
          Citizen.Wait(4)
          local coords = GetEntityCoords(GetPlayerPed(-1))
          local playerCoords = GetEntityCoords(PlayerPedId())
     
          if GetDistanceBetweenCoords(playerCoords, Config.Circles.FalsifyMoney.coords, true) < 1.0 and not connected then
               ESX.ShowHelpNotification(_U('tap_electricity'))
               if IsControlJustReleased(0, Config.Keys['E']) then
                    connected = true
                    itemRemoved = false
                    TaskStartScenarioInPlace(PlayerPedId(), 'world_human_stand_fishing', 0, false)

				Citizen.Wait(5000)
                    ClearPedTasks(PlayerPedId())

                    ESX.ShowNotification(_U('connect_machine'))
				Citizen.Wait(2000) 
                    ESX.ShowNotification(_U('counterfeiting_money'))
               end
          end

          if GetDistanceBetweenCoords(playerCoords, Config.Circles.FalsifyMoney.coords, true) < 1.0 and connected then
               ESX.ShowHelpNotification(_U('disconnect_machine')) 
               if IsControlJustReleased(0, Config.Keys['E']) then
                    connected = false
                    isPrinting = false
                    ESX.ShowNotification(_U('machine_disconnect'))
                    Citizen.Wait(2000)
               end
          end

          if connected == true then
               --sCitizen.Wait(3000)
               ESX.TriggerServerCallback('falsifyMoney:checkCableLenght', function(cableLenght)
                    if cableLenght == 5 and itemRemoved == false then
                         if GetDistanceBetweenCoords(playerCoords, Config.Circles.FalsifyMoney.coords, true) > 5.0 then
                              itemRemoved = true
                              TriggerServerEvent('printing:cable_broke')
                              connected = false
                         end
                    elseif cableLenght == 10 and itemRemoved == false then
                         if GetDistanceBetweenCoords(playerCoords, Config.Circles.FalsifyMoney.coords, true) > 10.0 then
                              itemRemoved = true
                              TriggerServerEvent('printing:cable_broke')
                              connected = false
                         end
                    elseif cableLenght == 20 and itemRemoved == false then
                         if GetDistanceBetweenCoords(playerCoords, Config.Circles.FalsifyMoney.coords, true) > 20.0 then
                              connected = false
                              itemRemoved = true
                              TriggerServerEvent('printing:cable_broke')
                         end
                    elseif cableLenght == nil then
                         connected = false
                         isPrinting = false
                         ESX.ShowNotification(_U('no_cable'))
                    end              
               end) 
          end

          if connected then
               if IsPedInAnyVehicle(GetPlayerPed(-1)) then
                    currentVehicle = GetVehiclePedIsUsing(PlayerPedId())
                    car = GetVehiclePedIsIn(GetPlayerPed(-1), false)
                    lastCar = GetVehiclePedIsUsing(GetPlayerPed(-1))
                    local model = GetEntityModel(currentVehicle)
                    local modelName = GetDisplayNameFromVehicleModel(model)
                    isInVehicle = true

                    if modelName == 'BURRITO' and car then
                         isInVehicle = true
                         if GetPedInVehicleSeat(car, -1) == GetPlayerPed(-1) then
                              if not isPrinting then
                                   ESX.ShowHelpNotification(_U('start_falsifymoney'))
                              end
                              if IsControlJustReleased(0, Config.Keys['E']) then
                                   OpenPaperPrintingMenu()
                                   isPrinting = true
                              end
                         end
                    end
               else
                    isInVehicle = false
               end
          end

          if isPrinting and not isInVehicle then
               isPrinting = false
               TriggerServerEvent('processing:stop')
          end

     end
end)

function OpenPaperPrintingMenu()
     ESX.UI.Menu.CloseAll()

     ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'paper_printing', {
		title    =  _U('paper_cutting'),
		align    = 'top-right',
		elements = {
               { label = 'A3', value = 'a3' },
               { label = 'A4', value = 'a4' },
               { label = 'A5', value = 'a5' },
          }
	}, function(data, menu)
		menu.close()
          menuOpen = false
          if data.current.value == 'a3' then
               TriggerServerEvent('printing:starta3')
               isPrinting = true
		end
		if data.current.value == 'a4' then
               TriggerServerEvent('printing:starta4')
               isPrinting = true
          end
          if data.current.value == 'a5' then
               TriggerServerEvent('printing:starta5')
               isPrinting = true
          end
          Citizen.Wait(4)
	end)
end

RegisterNetEvent('cable:broke')
AddEventHandler('cable:broke', function()
	TriggerServerEvent('processing:stop')
end)
