
function drawCircle(position, color, size, type)
    DrawMarker(type, position.x, position.y, position.z + 0.02, 0.0, 0.0, 0.0, 0, 0.0, 0.0, size.x, size.y, size.z, color.r, color.g, color.b, 100, false, true, 2, false, false, false, false)
end

-- Function to make peds --
-- WERKT NOG NIET MAAK IK MORGEN AF -- AUB DEZE BRANCHE NIET EDITEN --
function makePed(seller, model, position)
        Citizen.Wait(4)
        RequestModel(GetHashKey(model))
        while not HasModelLoaded(GetHashKey(model)) do
            Wait(1)
        end
        local npc = CreatePed(4, model, position.x, position.y, position.z, position.h, false, true)
        SetPedFleeAttributes(npc, 0, 0)
        SetPedDropsWeaponsWhenDead(npc, false)
        SetPedDiesWhenInjured(npc, false)
        SetEntityInvincible(npc , true)
        FreezeEntityPosition(npc, true)
        SetBlockingOfNonTemporaryEvents(npc, true)
        if seller then 
            RequestAnimDict("missfbi_s4mop")
            while not HasAnimDictLoaded("missfbi_s4mop") do
                Wait(1)
            end
            TaskPlayAnim(npc, "missfbi_s4mop" ,"guard_idle_a" ,8.0, 1, -1, 49, 0, false, false, false)
        else
            GiveWeaponToPed(npc, GetHashKey("weapon_assaultrifle"), 2800, true, true)
    end
end

RegisterNetEvent('main:notify')
AddEventHandler('main:notify', function(message)
    ESX.ShowNotification(message)
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(4)
        local playercoords = GetEntityCoords(PlayerPedId())
        for k,zone in pairs(Config.Circles) do
            if GetDistanceBetweenCoords(playercoords, zone.coords, true) < Config.DrawDistance then
                drawCircle(zone.marker, zone.color, zone.size, zone.markertype)
            end
        end
    end
end)

Citizen.CreateThread(function()
        Citizen.Wait(4)
        for k,v in pairs(Config.Peds) do
            makePed(v.seller, v.model, v.position)
    end
end)