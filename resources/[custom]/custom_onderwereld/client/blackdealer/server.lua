ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('blackdealer:buyItem')
AddEventHandler('blackdealer:buyItem', function(itemName, itemPrice, value)
    local xPlayer = ESX.GetPlayerFromId(source)
    totalPrice = ESX.Math.Round(tonumber(itemPrice * value))
    playerMoney = xPlayer.getMoney()   
    if playerMoney >= totalPrice then
        xPlayer.removeMoney(totalPrice)
        xPlayer.addInventoryItem(itemName, value)
        TriggerClientEvent('main:notify', source, _U('bought_item',value, itemName, ESX.Math.GroupDigits(totalPrice)))
    else
        TriggerClientEvent('main:notify', source, _U('not_enough_money'))
    end
end)