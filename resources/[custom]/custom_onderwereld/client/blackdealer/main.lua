ESX              = nil
local PlayerData = {}
local HasAlreadyEnteredMarker = false
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)


RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

RegisterCommand('coords', function()
	print(GetEntityCoords(PlayerPedId()))
end)

menuOpen = false

-- Supply Dealer --
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(4)
		local isInMarker = false
		local coords = GetEntityCoords(PlayerPedId())
		if GetDistanceBetweenCoords(coords, Config.Circles.SupplyDealer.coords, true) < 1.0 then
			isInMarker = true
			HasAlreadyEnteredMarker = true
			if not menuOpen then
				ESX.ShowHelpNotification(_U('open_supplydealer'))
				if IsControlJustReleased(0, Config.Keys['E']) then
					OpenSupplyDealer()
					menuOpen = true
				end
			end
		end	
		if not isInMarker and HasAlreadyEnteredMarker then
			menuOpen = false
			HasAlreadyEnteredMarker = false
			ESX.UI.Menu.CloseAll()
		end
	end
end)

function OpenSupplyDealer()
	ESX.UI.Menu.CloseAll()
	
	local elements = {}
	for k,v in pairs(Config.BlackDealer.SupplyDealer.elements) do
		local price = v
		table.insert(elements, {
			label = ('%s <span style="color:green;">%s</span>'):format(k, '€' .. ESX.Math.GroupDigits(price)),
			name = k,
			price = price
		 })
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'supplydealer', {
		title    = Config.BlackDealer.SupplyDealer.title,
		align    = Config.BlackDealer.SupplyDealer.align,
		elements = elements
	}, function(data, menu)
		if data.current.name == 'methlab' then
			TriggerServerEvent('blackdealer:buyItem', data.current.name, data.current.price, 1)
		else
			AmountSelect(data.current.name, data.current.price)
			print(data.current.name)
		end
		menu.close()
		menuOpen = false
		
	end, 
	function(data, menu)
		menu.close()
		menuOpen = false
	end)
end

function AmountSelectt(itemName, itemPrice)
	ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'lookup_vehicle', {
		title = 'Hoeveel ' .. itemName .. ' wil je kopen?',
	}, function(data, menu)
		if tonumber(data.value) == nil or data.value == nil then
			print('fout kanker debiel')
		else
			TriggerServerEvent('blackdealer:buyItem', itemName, itemPrice, data.value)
			print(data.value)
		end
		menu.close()
	end, function(data, menu)
		menu.close()
	end)
end
