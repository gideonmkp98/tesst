Locales['en'] = {

-- BlackDealer --
['supplydealer_title'] = 'Supply Dealer',
['open_supplydealer']  = 'Press ~INPUT_PICKUP~ to open the blackmarket',
['bought_item'] = 'You bought ~b~%s %s~s~ for ~r~€%s~s~',
['not_enough_money'] = 'You dont have ~r~enough~s~ money',


-- Money Laundering --
['open_moneylaundering'] = 'Press ~INPUT_PICKUP~ to launder money',
['amount_money'] = 'How much money do you want to launder?',
['laundery_finished'] = 'You have ~g~€%s~s~ left after the laundering ',

-- Counterfeiting Money --
['paper_printing'] = 'Paper Printing',
['printing_money'] = 'You are processing paper into fake money paper',
['tap_electricity'] = 'Press ~INPUT_PICKUP~ to drain the power of the building',
['connect_machine'] = 'You ~g~successfully ~w~connected the money print machine',
['counterfeiting_money'] = 'Go back to your van to start making counterfeiting money',
['start_falsifymoney'] = 'Press ~INPUT_PICKUP~ to start making counterfeiting money',
['disconnect_machine'] = 'Press ~INPUT_PICKUP~ to stop draining power form the building',
['machine_disconnect'] = 'You ~g~successfully ~w~disconnected the money print machine from the building',
['no_cable'] = 'You dont have a electricity cable with you to connect the money print machine',
['cable_broke'] = 'You went too far with the cable. You ~r~broke ~w~the cable',
['no_supplies_printing'] = '~r~You dont have enough supplies to print paper',

['paper_processing'] = 'Paper Processing',
['start_paperprocessing'] = 'Press ~INPUT_PICKUP~ to start making paper',
['processing_paper'] = 'You are processing cotton fibers into paper',
['cant_hold_paper'] = '~r~You have too much paper with you',
['no_supplies_process'] = '~r~You dont have enough supplies to process paper',
['stopped_paperprocessing'] = 'You ~r~stopped ~w~making paper',

['paper_cutting'] = 'Paper Cutting',
['start_cutting'] = 'Press ~INPUT_PICKUP~ to start cutting the Fakemoney Papers',
['cutting_money'] = 'You are cutting fake money paper into fake money biljets',
['no_supplies_cutting'] = '~r~You dont have enough supplies to cut fake money paper',

['fake_money_selling'] = 'Fake Money Selling',
['start_selling'] = 'Press ~INPUT_PICKUP~ to sell your fake money for black money',
['no_supplies_selling'] = '~r~You dont have enough fake money biljets to sell',

}