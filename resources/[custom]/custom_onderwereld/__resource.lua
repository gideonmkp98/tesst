resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'Custom Onderwereld'

server_scripts {
	'@es_extended/locale.lua',
	'locales/en.lua',
	'config.lua',
	'server/main.lua',
	'client/blackdealer/server.lua',
	'client/moneylaundering/server.lua',
	'client/geldvervalsen/server.lua'
}

client_scripts {
	'@es_extended/locale.lua',
	'locales/en.lua',
	'config.lua',
	'client/main.lua',
	'client/geldvervalsen/main.lua',
	'client/geldvervalsen/processing.lua',
	'client/geldvervalsen/cutting.lua',
	'client/geldvervalsen/selling.lua',
	'client/blackdealer/main.lua',
	'client/moneylaundering/main.lua'
}

dependencies {
	'es_extended'
}