Config = {}

Config.Locale = 'en'

Config.Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

 -- BlackDealer --

Config.DrawDistance = 100

Config.Circles = {
     SupplyDealer = { 
          coords =       vector3(2676.146, 3502.135, 53.3033),
          color =        { r = 0, g = 0, b = 0 },
          size =         { x = 1.5, y = 1.5, z = 1.0 },
          marker =       { x = 2676.146, y = 3502.135, z = 52.3033},
          markertype =   1,
     },
     WeaponDealer = { 

          coords =        vector3(2671.165, 3522.251, 52.65711),
          color =        { r = 0, g = 0, b, 225 },
          size =         { x = 1.5, y = 1.5, z = 1.0},
          marker =       { x = 2671.165, y = 3522.251, z = 51.65711 },
          markertype =   1,
     },
     MoneyLaundering = { 
          coords =       vector3(1984.237, 3049.418, 47.215),
          color =        { r = 255, g = 0, b = 208 },
          size =         { x = 1.5, y = 1.5, z = 1.0},
          marker =       { x = 1984.237, y = 3049.418, z = 46.215 },
          markertype =   1,
     },
     MoneyLaundering2 = { 
          coords =       vector3(-2174.805, 4294.516, 49.0517),
          color =        { r = 51, g = 204, b = 51 },
          size =         { x = 1.5, y = 1.5, z = 1.0},
          marker =       { x = -2174.805, y = 4294.516, z = 48.0517 },
          markertype =   1,
      },

     FalsifyMoney = { 
          coords =       vector3(602.7153, 2784.62, 42.21121),
          color =        { r = 0, g = 0, b = 0 },
          size =         { x = 1.5, y = 1.5, z = 1.0},
          marker =       { x = 602.7153, y = 2784.62, z = 41.21121 },
          markertype =   25,
     },
     PaperProcessing = { 
          coords =       vector3(-601.9946, -1602.715, 30.40517),
          color =        { r = 0, g = 0, b = 0 },
          size =         { x = 1.5, y = 1.5, z = 1.0},
          marker =       { x = -601.9946, y = -1602.715, z = 29.40517 },
          markertype =   25,
     },
     PaperCutting = { 
          coords =       vector3(712.9202, -972.3157, 30.39534),
          color =        { r = 0, g = 0, b = 0 },
          size =         { x = 1.5, y = 1.5, z = 1.0},
          marker =       { x = 712.9202, y = -972.3157, z = 29.39534 },
          markertype =   1,
     },
     FakeMoneySelling = { 
          coords =       vector3(260.6714, 204.9407, 110.287),
          color =        { r = 0, g = 0, b = 0 },
          size =         { x = 1.5, y = 1.5, z = 1.0},
          marker =       { x = 260.6714, y = 204.9407, z = 109.287 },
          markertype =   25,

     }
}

Config.Peds = {
     SupplyDealerPed = {
          seller = true,
          model = "csb_avon",
          position = { x= 2676.318, y= 3501.407, z= 52.30344, h= 20.0 }
     },
     SupplyDealerPed2 = {
          seller = false, 
          model = "csb_mweather", 
          position = {x = 2678.13,  y = 3499.782,  z = 52.30383, h = 35.0}
     },
     SupplyDealerPed3 ={
          seller = false, 
          model = "csb_mweather", 
          position = {x = 2675.877,  y = 3499.288,  z = 52.30331, h = 5.0}
     },
}

Config.BlackDealer = { 
     SupplyDealer = {
          title = _U('supplydealer_title'),
          align = 'top-right',
          elements = {
               Accetone =               50,
               Lithium  =               80,
               Methlab  =               20000,
               Cotton_Fibers =          20,
               a5money_print_machine = 100000,
               a4money_print_machine = 400000,
               a3money_print_machine = 1000000,
               cotton_fibers = 0.10,
               electricity_cable5m = 30,
               electricity_cable10m = 60,
               electricity_cable20m = 90,
          }
     }
}

Config.MoneyLaundering = {
     Location1 = {
          coords = vector3(1984.237, 3049.418, 47.215),
          tax = 0.9,
     },
     Location2 = {
          coords = vector3(-2174.805, 4294.516, 49.0517),
          tax = 0.5,
     },
}
