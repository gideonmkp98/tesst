ESX = nil

if Config.UseESX then
	TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

	RegisterServerEvent('fuel:pay')
	AddEventHandler('fuel:pay', function(price)
		local xPlayer = ESX.GetPlayerFromId(source)
		local amount = ESX.Math.Round(price)
		local discord_webhook = {
			url = "https://discordapp.com/api/webhooks/705480101575393340/tBWtEI2Na7mLiGxOLd--IKmdqY9Hee4AheloodxsSYzElqF-QvWwkn7QGYA9Bol9fI7h",
			image = "https://cdn.discordapp.com/attachments/687387039527862368/703973684695531650/Logo_maarseveen.png"
		}

		if price > 0 then
            xPlayer.removeMoney(amount)
			TriggerClientEvent('esx:showNotification', source, '~w~Je hebt ~r~€' .. amount .. '~w~ betaald!')
			AddEventHandler('fuel:pay', function() 
				PerformHttpRequest(discord_webhook.url, 
				function(err, text, headers) end, 
				'POST',
				json.encode({username = DISCORD_NAME, content = "```CSS\n".. GetPlayerName(source) .. " heeft voor [auto] getankt voor €" .. amount .. "\n```", avatar_url = DISCORD_IMAGE}), { ['Content-Type'] = 'application/json' })  
			end)
		end
	end)
end
