(() => {
	UTILS = {};

	UTILS.parseFiveMFormatting = function(text) {
		var closeQueue = [];
		text = text.replace(/~n~/gi,'<br>');
		text = text.replace(/~[rbgypcmuow]~/gi,'');
		function recurseFormat(text) {
			let index = text.search(/~[rbgypcmuoshw]~/i);
			if (index > -1) {
				// repetitive code possibly use function
				switch(text[index+1]) {
					case 'h':
						closeQueue.push('</strong>');
						text = text.substr(0,index)+'<strong>'+text.substr(index+3);
						break;
					case 's':
						let closeTags = '';
						while (closeQueue.length > 0) {
							closeTags += closeQueue.pop();
						}
						text = text.substr(0,index)+closeTags+text.substr(index+3);
						break;
				}
				text = recurseFormat(text);
			} else {
				let closeTags = '';
				while (closeQueue.length > 0) {
					closeTags += closeQueue.pop();
				}
				text = text+closeTags;
			}
			return text;
		}
		text = recurseFormat(text);
		return text;
	};

	UTILS.parseInt = function(data, defaultValue) {
		let number = parseInt(data);
		if (isNaN(number)) number = defaultValue;
		return number;
	}
})();
