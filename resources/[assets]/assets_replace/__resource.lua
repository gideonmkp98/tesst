resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

files {
	'stream/fbi/FBI.xml',
	'stream/fbi2/FBI2.xml',
	'stream/police3/POLICE3.xml',
	'stream/police/POLICE.xml',
	'stream/police4/POLICE4.xml',
	'stream/sheriff/SHERIFF.xml',
	'stream/police2/POLICE2.xml',
	'stream/riot/RIOT.xml',
	'stream/riot2/RIOT2.xml',
	'stream/policeold2/POLICEOLD2.XML',
	'stream/policeb/POLICEB.xml',
	'stream/firetruk/FIRETRUK.xml',
	'handling.meta',
	'carvariations.meta',
}

data_file 'HANDLING_FILE' 'handling.meta'
data_file 'VEHICLE_VARIATION_FILE' 'carvariations.meta'

is_els 'true'

