resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

files {
    'vehicles.meta',
    'carvariations.meta',
    'carcols.meta',
    'handling.meta',
    'stream/[politie]/vwgolfunmarked/VWGOLFUNMARKED.xml',
    'stream/[politie]/eklasseunmarked/EKLASSEUNMARKED.xml',
    'stream/[politie]/polme/POLME.xml',
    'stream/[politie]/polvoa/POLVOA.xml',
    'stream/[ambulance]/ambulance1/AMBULANCE1.xml',
    'stream/[ambulance]/ambulance5/AMBULANCE5.xml',
    'stream/[anwb]/anwbcaddy/ANWBCADDY.xml',
    'stream/[ambulance]/sprinterambu/SPRINTERAMBU.xml',
    'stream/[politie]/ptouraeg/ptouraeg.xml',
    'stream/[politie]/paudi/paudi.xml',
    'stream/[politie]/pbklasse/PBKLASSE.xml',
    'stream/[politie]/ptouran/PTOURAN.xml',
    'stream/[politie]/ptouran07/PTOURAN07.xml',
    'stream/[rijkswaterstaat]/rwstoyota/RWSTOYOTA.xml',
    'stream/[rijkswaterstaat]/tekstkar/TEKSTKAR.xml',
    'stream/[ambulance]/ambuvolvo/AMBUVOLVO.xml',
    'stream/[ambulance]/ambuq7/AMBUQ7.xml',
    'stream/[ambulance]/ambutouran/AMBUTOURAN.xml',
    'stream/[politie]/v70unmarked/V70UNMARKED.xml',
    'stream/[politie]/v70marked/V70MARKED.xml',
    'stream/[anwb]/flatbed3/FLATBED3.xml',
    'stream/[politie]/pyamaha/PYAMAHA.xml',
    'stream/[politie]/T6/T6.xml',
    'stream/[politie]/T5/T5.xml',
    'stream/[politie]/T617/T617.xml',
    'stream/[ambulance]/ambumotor/AMBUMOTOR.xml',
	'stream/[politie]/ppassat/PPASSAT.xml',
    'stream/[politie]/pbmwr1200/PBMWR1200.xml',
    'stream/[politie]/politiemotorlicht/POLITIEMOTORLICHT.xml',
	'stream/[anwb]/anwbvito/ANWBVITO.xml',
    'stream/[politie]/pvito/PVITO.xml',
    'stream/[politie]/bmwunmarked/BMWUNMARKED.xml',
	'stream/[politie]/clsunmarked/CLSUNMARKED.xml',
	'stream/[politie]/v250unmarked/V250UNMARKED.xml',
	'stream/[brandweer]/brandweer/BRANDWEER.xml',
	'stream/[brandweer]/brandweer2/POLICE.xml',
	'stream/[brandweer]/brandweer3/BRANDWEER3.xml',
	'stream/[brandweer]/brandweer2/BRANDWEER2.xml',
	'stream/[brandweer]/brandweer4/BRANDWEER4.xml',
	'stream/[ambulance]/ambuamarok/AMBUAMAROK.xml',
	'stream/[ambulance]/policeb/POLICEB.xml',
	'stream/[politie]/unmarkedtrans/unmarkedtrans.xml',
	'stream/[politie]/pbklassenew/pbklassenew.xml',
	'stream/[ambulance]/ambucrafter/AMBUCRAFTER.xml',
	'stream/[politie]/Q7/POLICE4.xml',
	'Stream/[politie]/PolitieBoot/dinghy2.xml',
	'Stream/[politie]/police4/POLICE4.xml',
}

data_file 'HANDLING_FILE' 'handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'carvariations.meta'
data_file 'CARCOLS_FILE' 'carcols.meta'

client_script {
    'vehicle_names.lua'    -- Not Required
}

is_els 'true'
