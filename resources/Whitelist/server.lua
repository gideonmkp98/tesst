----------------------------------------
--- Discord Whitelist, Made by FAXES ---
----------------------------------------

--- Config ---
notWhitelisted = "Je bent niet geverifieerd op de Maarsseveen Roleplay Discord!" -- Message displayed when they are not whitelist with the role
noDiscord = "Join onze discord voordat je op de server gaat: bit.ly/maarsseveenrp" -- Message displayed when discord is not found
roleNeeded = "Inwoner"

--- Code ---

AddEventHandler("playerConnecting", function(name, setCallback, deferrals)
    local src = source
    deferrals.defer()
    deferrals.update("Checking Permissions")

    for k, v in ipairs(GetPlayerIdentifiers(src)) do
        if string.sub(v, 1, string.len("discord:")) == "discord:" then
            identifierDiscord = v
        end
    end

    if identifierDiscord then
            if exports.discord_perms:IsRolePresent(src, roleNeeded) then
                deferrals.done()
            else
                deferrals.done(notWhitelisted)
        end
    else
        deferrals.done(noDiscord)
    end
end)